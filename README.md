Snake with React.js
==========

[![pipeline status](https://gitlab.com/nav/the-snake-pipeline/badges/master/pipeline.svg)](https://gitlab.com/nav/the-snake-pipeline/commits/master)

Multiplayer Snake game (met ReactJS en CI via Gitlab)
- Node & ReactJS based game
- Unit testing with Jest: http://facebook.github.io/jest/

Installation
-----
NodeJs
-----
1. Clone het project naar je lokale omgeving met de git clone command:
```bash
git clone https://gitlab.com/nav/the-snake-pipeline.git
```
 
2. Vervolgens installeer je de benodigde packages met npm:
```bash
npm install
```

3. Nu kun je de node server starten met de command:
```bash
npm start
```

Docker
-----
1. Clone het project.
```bash
git clone https://gitlab.com/nav/the-snake-pipeline.git
```

2. installeer docker.
```bash
https://www.docker.com/
```

3. nu kun je de omgeving starten met de command:
``` bash
docker-compose up -d 
```


Todo's
-----
- ~~Add a startscreen, scoreboard, and database for the game~~
- ~~Save the usernames and scores to a central database~~ 
- ~~Make the game multiplayer (with scores/leaderboard)~~
- ~~Add docker to this project (for CI)~~
- ~~Setup gitlab pipeline for CI (building and testing)~~
- ~~Setup pipeline for deployment (for CD)~~
- ~~Adding umit-tests & making it work properly~~

Some other important tasks:
- Release plan maken (https://drive.google.com/file/d/0BwPRr3jxbad7V2RNVkFjQ3NGQm8/view)
- You need 2 pipelines:
	- CI: run the build (docker) & tests triggered on all commits that has been pushed to gitlab.
	- CD: if change passes integration tests in the CI-pipeline, then deploy it.

Running
-----
Make sure you got node available, and get up and running with:
```bash
npm start
```
This will start the node server on port 3000, located here: http://localhost:3000/. 


Testing 
-----

Basic Unit Tests with Jest.
To run them:
```bash
npm run test:unit
```
or
```bash
npm run test:app
```

End-to-End tests using [Nightwatch.js](http://nightwatchjs.org/) (with Selenium).
To run them:
```bash
npm run test:end-to-end
```
