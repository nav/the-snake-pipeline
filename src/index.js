import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import FirebaseDB from './FirebaseDB'

var db = new FirebaseDB();
var highscore = db.getHighScores();

var start = document.getElementById("startbutton");
var page = document.getElementById("mainpage");
var root = document.getElementById("game");

// Login variables 
var create = document.getElementById("create");
var login = document.getElementById("login");

// Highscores
var highscores = document.getElementById("currenthighscores");
// Handle login/logout
db.handleLoginLogout();

root.hidden = true;
start.onclick = startGame;

create.onclick = db.writeUserData;

login.onclick = function () {
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    function success(bla){
        var divFormErrors = document.getElementById('form-errors');
        divFormErrors.innerHTML = '';
    }
    function fail(result) {
        var errorMessage = result.message;
        var divFormErrors = document.getElementById('form-errors');
        divFormErrors.innerHTML = '<div class="alert alert-danger" role="alert">' + errorMessage + '</div>';
    }
    db.userSignIn(email, password, success, fail);
};

highscores.innerHTML = db.getHighScores();

function startGame() {
    root.hidden = false;
    page.hidden = true;
    ReactDOM.render(<App />, root);
    registerServiceWorker();    
}




