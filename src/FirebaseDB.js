import firebase from 'firebase'

var rootRef;
var app;
export default class FirebaseDB {

    constructor(){
        var config = {
            apiKey: "AIzaSyDAWchiPYAGbt5hLHbsLEAtfGc60PZwT6Q",
            authDomain: "the-snake-pipeline.firebaseapp.com",
            databaseURL: "https://the-snake-pipeline.firebaseio.com"              
            };
            
            app = firebase.initializeApp(config);
            rootRef = firebase.database().ref('highscores');
            console.log("db connected " + app.name);
    }

        // showUsers(){
        //     console.log("Hel yeah");
        //
        //     rootRef.on("value", function(snapshot){
        //         var users = snapshot.val();
        //         for (var i = 1; i < users.length; i++) {
        //            console.log(users[i].keys());
        //        }
        //
        //      }
        //    );
        // }

        writeUserData() {
            var email = document.getElementById("email").value;
            var password = document.getElementById("password").value;

            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function(val) {
                console.log("logged in" + 
                firebase.auth().currentUser.uid);

                var divFormErrors = document.getElementById('form-errors');
                    divFormErrors.innerHTML = '';
            }
            ,function(error) {
                // Handle Errors here.
                // var errorCode = error.code;
                // console.log(errorCode + " " + errorMessage);

                    var errorMessage = error.message;
                    var divFormErrors = document.getElementById('form-errors');
                    divFormErrors.innerHTML = '<div class="alert alert-danger" role="alert">' + errorMessage + '</div>';
              });
        }

        userSignIn(email, password, callback, callbackfail){

            firebase.auth().signInWithEmailAndPassword(email, password).then(
                function(val) {
                    // console.log("logged in" +
                    // firebase.auth().currentUser.uid);
                    // usermanagement.hidden = true;
                    // alert("You are now logged in " + firebase.auth().currentUser.email);
                    callback(true);
                },
                function(error) {
                    // var errorCode = error.code;
                    // console.log(errorCode + " " + errorMessage);
                    callbackfail(error);
                });
        }

        getHighScores(){
            // var currentUserEmail = firebase.auth().currentUser.email;
            var highScores = [];

            rootRef.on("value", function(snapshot){
                //var highscores = snapshot.val();
                // console.log(highscores);

                snapshot.forEach(function(child) {
                    highScores.push({user:child.val().email, score:child.val().score});
                  });

                highScores = highScores.sort(function(a, b){return b.score-a.score}).slice(0, 5);
                console.log(highScores);

                var stringresult = "";
            
                    highScores.forEach(function(element) {
                        stringresult += '<div class="list-group-item clearfix"><div class="pull-left"><h4 class="list-group-item-heading">' + element.score +' </h4> <p class="list-group-item-text"> '+ element.user+' </p></div></div>'
                    }, this);
                    console.log(stringresult);
                    var highscores = document.getElementById("currenthighscores");
                    highscores.innerHTML = stringresult;
             }
           ); 
            
        }

    handleLoginLogout(){

        var signOutBtn = document.getElementById('signOut');
        var signInBtn = document.getElementById('signIn');
        var welcomeTxt = document.getElementById('welcomeTxt');
        var wrap = document.getElementById('wrap');

        firebase.auth().onAuthStateChanged(function(user){
            if(user){

                signOutBtn.classList.remove('hide');
                signOutBtn.addEventListener('click', function(event) {
                    firebase.auth().signOut();
                });
                signInBtn.classList.add('hide');
                welcomeTxt.innerHTML = 'Welcome, ' + user.email;
                wrap.classList.remove('active');
                wrap.classList.add('wrap');
                showStartBtn(true);

            }else{
                signOutBtn.classList.add('hide');
                signInBtn.classList.remove('hide');
                welcomeTxt.innerHTML = 'Please login to play game..';
                showStartBtn(false);

            }

            function showStartBtn(show)
            {
                var startBtn = document.getElementById('startbutton');
                if (show === true){
                    startBtn.removeAttribute("disabled");
                    startBtn.classList.remove('hide');
                } else {
                    startBtn.setAttribute("disabled", true);
                    startBtn.classList.add('hide');

                }
            }
        });
    }
}
