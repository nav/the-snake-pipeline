import React from 'react';
import ReactDOM from 'react-dom';
import FirebaseDB from '../FirebaseDB'


describe('firebase connection tests', () =>{
    var db;

    beforeAll(()=>{
        db = new FirebaseDB()
    });

    afterAll(()=>{
        db.database().goOffline();
    });

    it('wrong password entered', done => {
        function testResult(data) {
            expect(data.code).toBe("auth/wrong-password");
            done();
        }
        db.userSignIn('user@test.nl','1123456', null, testResult)
    });

    it('wrong email formatting', done => {
        function testResult(data) {
            expect(data.code).toBe('auth/invalid-email');
            done();
        }
        db.userSignIn('user@test..nl','123456', null, testResult)
    });

    it('wrong email', done => {
        function testResult(data) {
            expect(data.code).toBe('auth/user-not-found');
            done();
        }
        db.userSignIn('testtest@test.nl','123456', null, testResult)
    });

    it('successful login', done => {

        function testResult(data){
            expect(data).toBe(true);
            done();
        }
        db.userSignIn('user@test.nl','123456', testResult);
    });



});