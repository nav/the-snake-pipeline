const commands = {
  inputTestValues: function inputTestValues(browser) {

    // Gitlab CI/CD project for DINF4 - Nav 26-11-2017
    // Give the game some time to get ready. 
    this.api.pause(1000);

    // Expecting the intro-divider with the image to be present
    browser.expect.element('.intro-divider').to.be.present;

    // startbutton should be present on the startpage
    browser.expect.element('#startbutton').to.be.present;

    // The highscores should be present on the startpage
    browser.expect.element('#highscores').to.be.present;

    // Title in section-heading should be present on the startpage
    browser.expect.element('#wrap').to.be.present;

    return this;
  },
};

module.exports = {
  commands: [commands],
  elements: {},
};
