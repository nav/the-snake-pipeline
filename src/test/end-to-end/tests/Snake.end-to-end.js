module.exports = {
  'Snake page': function snakeGameShouldPlay(browser) {
    browser
      .url('http://localhost:3000')
      .windowMaximize();

    const snakePage = browser.page.snakePage();
    snakePage.inputTestValues(browser);

    browser.end();
  },
    'Login as user test@test.nl' : function (browser) {
        browser
            .url('http://localhost:3000')
            .pause(3000)
            .click('#signIn')
            .setValue('#email', 'user@test.nl')
            .setValue('#password', '123456')
            .click('#login')
            .pause(3000)
            .assert.containsText('#welcomeTxt', 'Welcome, user@test.nl')
            .screenshot('user-logged-in.png')
            .end();
    }
};
